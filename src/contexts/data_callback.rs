callback! {
    struct DataCallback {
        /// Data from the callback.
        data: String,
    } -> EventLoop::data_callback
}
